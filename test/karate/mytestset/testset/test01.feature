Feature: test 01

Background:
  * def apiHost = (karate.properties['apiHost'] ? karate.properties['apiHost'] : 'localhost')
  * def apiPort = (karate.properties['apiPort'] ? parseInt(karate.properties['apiPort']) : 8080)

  * def host = (karate.env == 'mock' ? 'localhost' : apiHost)
  * def port = (karate.env == 'mock' ? karate.start('../mock/test01-mock.feature').port : apiPort)
  * def basePath = ''
  * def apiUrl = 'http://' + host + ':' + port + basePath

  * url apiUrl

  * def sum = function(sum, element){ return sum + element }


Scenario: create test 01
  * def newPost = 
      """
      {
        post: {
          title: "test1",
          author: "aaa"
        }
      }
      """
  # 他クラスのメソッド呼び出しと同じ感じでシナリオを実行
  * def result = call read('../util/test01-util.feature@name=postOnePost') newPost
  * def response = $result.response
  * print 'response: ', response

  * def id = response.id
  * print 'id: ', id

  * def result = call read('../util/test01-util.feature@name=postOnePost') newPost
  * def response = $result.response
  * print 'response: ', response

  Given path 'posts'
  When method get
  Then status 200
  * print 'response: ', response
  
  * def posts = response

  # 呼び出し元で def で定義された変数はそのまま利用可能、配列を引数にした場合は要素分繰り返し処理
  * def result = call read('../util/test01-util.feature@name=getPostById') posts
  * def response = $result[*].response
  * print 'response: ', response

  * def responseTimes = $result[*].response.responseTime
  * print 'responseTimes: ', responseTimes

  # javascript の関数はそのまま使える
  * def sumTimes = responseTimes.reduce(sum, 0);
  * print 'sumTimes: ', sumTimes

  * def targetIds = [ {id: 1}, {id: 2} ]
  * def targetNames = [ {name: 'aaa'}, {name: 'bbb'}, {name: 'ccc'} ]
  * def targets = targetIds.map( function(currentValue, index, array) { return { id: currentValue.id, names: this } }, targetNames )
  * print 'targets: ', targets
  * def result = call read('../util/test01-util.feature@name=getPostByIdWithNames') targets
  * def response = $result[*].response
  * print 'response: ', response

  Given path 'posts/' + id
  When method delete
  Then status 200
  And match response == {}
  * print responseTime



