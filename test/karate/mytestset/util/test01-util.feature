Feature: Utilities for test 01

Background:
  * url apiUrl

@name=postOnePost
Scenario: post one post
  Given path 'posts/'
    And request post
  When method post
  Then status 201
  
  * print 'responseTime: ', responseTime
  * set response.responseTime = responseTime

@name=getPostById
Scenario: get post by id
  Given path 'posts/' + id
  When method get
  Then status 200

  * print 'responseTime: ', responseTime
  * set response.responseTime = responseTime

@name=getPostByIdWithNames
Scenario: get post by id and names
  * print 'id: ', id
  * print 'names: ', names

  * def result = call read('../util/test01-util.feature@name=getPostByIdWithName') names

@name=getPostByIdWithName
Scenario: get post by id and name
  * print 'id: ', id
  * print 'name: ', name

