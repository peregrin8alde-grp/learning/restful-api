Feature: test01 mock server

Background:
  * def basePath = ''
  #* configure cors = true
  #* def uuid = function(){ return java.util.UUID.randomUUID() + '' }
  * def curId = 0
  * def nextId = function(){ return ~~curId++ }
  * def posts = []
  * karate.log('posts : ', posts)

Scenario: pathMatches(basePath + '/posts') && methodIs('post')
  * karate.log(posts)
  * def post = request
  * def id = nextId()
  * post.id = id
  * karate.appendTo(posts, post)
  * karate.log('posts : ', posts)
  * def response = post
  * def responseStatus = 201

Scenario: pathMatches(basePath + '/posts')
  * karate.log(posts)
  * def response = posts

Scenario: pathMatches(basePath + '/posts/{id}') && methodIs('get')
  * karate.log('posts : ', posts)
  * def response = karate.jsonPath(posts, "$[?(@.id==" + pathParams.id + ")]")[0]
  * def responseStatus = 200

Scenario: pathMatches(basePath + '/posts/{id}') && methodIs('delete')
  * karate.log('posts : ', posts)
  * karate.remove('posts', '$[?(@.id==' + pathParams.id + ')]')
  * karate.log('posts : ', posts)
  * def response = {}
  * def responseStatus = 200

Scenario:
  * def responseStatus = 404

