# Karate

https://github.com/intuit/karate

## Standalone JAR

```
curl -L -o karate.jar https://github.com/intuit/karate/releases/download/v1.0.1/karate-1.0.1.jar

java -jar karate.jar -h
```

### The World's Smallest MicroService

```
docker run \
  -u $(id -u):$(id -g) \
  --name karate-cats-mock \
  -d \
  -p 8080 \
  -v "$PWD":/usr/src/myapp \
  -w /usr/src/myapp \
  openjdk:8 \
    java -jar karate.jar -m sample/cats-mock.feature -p 8080

docker network create restful_nw
docker network connect restful_nw karate-cats-mock

docker run \
  -u $(id -u):$(id -g) \
  -it --rm \
  -v "$PWD":/usr/src/myapp \
  -w /usr/src/myapp \
  --network=restful_nw \
  openjdk:8 \
    java \
      -Dmock.cats.url='http://karate-cats-mock:8080/cats' \
      -jar karate.jar \
        sample/cats.feature
```


### my testset


```
# standalone
docker run \
  -u $(id -u):$(id -g) \
  -it --rm \
  -v "$PWD":/usr/src/myapp \
  -w /usr/src/myapp \
  openjdk:8 \
    java \
      -DapiPort=8888 \
      -jar karate.jar \
        -e mock \
        mytestset/testset/test*.feature
```

