# JSON Server

https://github.com/typicode/json-server

- 簡易 Restful API サーバー
- サーバのモックとして利用

## install

```
docker run \
  --rm \
  -it \
  -v jsonserver:/usr/local \
  node:16 \
    npm install -g json-server
```

## 起動

`--host` を指定しないと外部からアクセス不可

```
docker run \
  --name jsonserver1 \
  -u "node" \
  -d \
  -p 3000 \
  -v jsonserver:/usr/local \
  -v $(pwd)/data:/home/node/app \
  -w "/home/node/app" \
  node:16 \
    json-server --watch db.json --host 0.0.0.0 --port 3000

docker network create restful_nw
docker network connect restful_nw jsonserver1
```

```
docker run \
  --rm \
  --network=restful_nw \
  node:16 \
    curl -X GET -sS \
      http://jsonserver1:3000/posts
```
